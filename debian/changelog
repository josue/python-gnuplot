python-gnuplot (1.8-7) UNRELEASED; urgency=medium

  * Update uploaders email
  * Add patch to port package to Python 3

 -- Josue Ortega <josue@debian.org>  Thu, 22 Aug 2019 19:14:41 -0600

python-gnuplot (1.8-6) unstable; urgency=medium

  * debian/copyright: Fixed file-without-copyright-information lintian
    warning.
  * Update Standards-Version to 3.9.6. No changes required.
  * debian/patches/fix-privacy-breach.patch added in order to avoid
    possible privacy breach
  * debian/rules: Added NEWS.txt as upstream changelog

 -- Josue Ortega <josueortega@debian.org.gt>  Sun, 24 May 2015 14:47:03 -0600

python-gnuplot (1.8-5) unstable; urgency=low

  * Update Standards-Version to 3.9.5. No changes required.
  * debian/control: VCS-* fields updated to cannonical URI.

 -- Josue Ortega <josueortega@debian.org.gt>  Sun, 05 Jan 2014 19:41:47 -0600

python-gnuplot (1.8-4) unstable; urgency=low

  * Upload to unstable

 -- Josue Ortega <josueortega@debian.org.gt>  Mon, 27 May 2013 00:29:42 -0600

python-gnuplot (1.8-3) experimental; urgency=low

  [ Josue Ortega ]
  * New Maintainer. (Closes: #699039)
  * Added debian/source/format file
  * Changed to dpkg-source 3.0 (quilt) format.
  * debian/copyright updated to format version 1.0
  * debian/control: added homepage.

  [ Anton Gladky ]
  * Use debhelper 9.
  * Remove XB-Python-Version.
  * Remove old postinst and prerm scripts.
  * Clean completely debian/rules.
  * Depend on gnuplot. python-gnuplot does not work without it.
  * Move the package into Debian-Science team.

 -- Josue Ortega <josueortega@debian.org.gt>  Sat, 16 Feb 2013 15:22:25 -0600

python-gnuplot (1.8-2) unstable; urgency=low

  * QA upload.
  * Orphan the package.
  * Build using dh_python2 instead of dh_pycentral. Closes: #617007.
  * Recommend gnuplot instead of depending on it. Closes: #600349.
  * Avoid string exceptions. Closes: #585295.
  * Fix lintian warnings.

 -- Matthias Klose <doko@debian.org>  Sat, 26 Jan 2013 17:42:40 +0100

python-gnuplot (1.8-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "manipulates site-packages/ directly, failing with Python 2.6"
    Applied and uploaded Kumar's patch. (Closes: #547842)

 -- Bastian Venthur <venthur@debian.org>  Sat, 07 Nov 2009 12:44:48 +0100

python-gnuplot (1.8-1) unstable; urgency=low

  * New upstream version.
    - doc file with broken link not shipped anymore. Closes: #336422.
    - Use `with_' instead of `with', which is a keyword in python2.6.
      Closes: #424965.
  * Use python-numpy instead of python-numeric. Closes: #469313, #478468.
  * Update watch file. Closes: #450271.
  * Register documention in section `Programming/Python'.
  * Update datafile.py.

 -- Matthias Klose <doko@debian.org>  Tue, 24 Jun 2008 18:40:22 +0200

python-gnuplot (1.7-7) unstable; urgency=low

  * Fix unhandled exception (Michael Schieschke). Closes: #353642.

 -- Matthias Klose <doko@debian.org>  Sun, 29 Oct 2006 02:45:58 +0200

python-gnuplot (1.7-6) unstable; urgency=low

  * Convert to the updated Python policy. Closes: #373542.

 -- Matthias Klose <doko@debian.org>  Fri, 16 Jun 2006 20:56:37 +0000

python-gnuplot (1.7-5) unstable; urgency=low

  * Fix mouse control in generated plot windows (closes: #291294).

 -- Matthias Klose <doko@debian.org>  Sun, 23 Jan 2005 00:42:37 +0100

python-gnuplot (1.7-4) unstable; urgency=low

  * Add watch file.

 -- Matthias Klose <doko@debian.org>  Sun, 19 Sep 2004 20:55:54 +0200

python-gnuplot (1.7-3) unstable; urgency=low

  * Fix typo in README.Debian (closes: #219485).

 -- Matthias Klose <doko@debian.org>  Sun, 16 Nov 2003 12:36:26 +0100

python-gnuplot (1.7-2) unstable; urgency=low

  * Reflect license change from GPL to LGPL.

 -- Matthias Klose <doko@debian.org>  Thu, 30 Oct 2003 22:38:29 +0100

python-gnuplot (1.7-1) unstable; urgency=low

  * New upstream version.
    - Allows pipes to be used for communication with gnuplot (closes: #211875).
    - Fixes syntax warning with python2.3 (closes: #216129).

 -- Matthias Klose <doko@debian.org>  Mon, 20 Oct 2003 00:17:07 +0200

python-gnuplot (1.6-4) unstable; urgency=low

  * Upload for python2.3 as the default python version.

 -- Matthias Klose <doko@debian.org>  Fri,  8 Aug 2003 07:53:03 +0200

python-gnuplot (1.6-3) unstable; urgency=low

  * Fix README (closes: #189508).
  * Shutup SyntaxWarning in 'oldplot.py' compat module (closes: #159077).

 -- Matthias Klose <doko@debian.org>  Sat, 10 May 2003 15:45:34 +0200

python-gnuplot (1.6-2) unstable; urgency=low

  * Build using python2.2.

 -- Matthias Klose <doko@debian.org>  Tue, 27 Aug 2002 22:36:50 +0200

python-gnuplot (1.6-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Thu, 22 Aug 2002 20:50:06 +0200

python-gnuplot (1.5-3) unstable; urgency=low

  * Build-depend on python-numeric (closes: #142080).

 -- Matthias Klose <doko@debian.org>  Thu, 11 Apr 2002 08:52:48 +0200

python-gnuplot (1.5-2) unstable; urgency=low

  * Add debhelper build dependency (closes: #139658).

 -- Matthias Klose <doko@debian.org>  Mon,  1 Apr 2002 14:35:20 +0200

python-gnuplot (1.5-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Sun, 30 Dec 2001 22:48:36 +0100

python-gnuplot (1.4-3) unstable; urgency=low

  * Update for python2.1.

 -- Matthias Klose <doko@debian.org>  Tue, 13 Nov 2001 00:00:48 +0100

python-gnuplot (1.4-2) frozen unstable; urgency=low

  * Add cascading style sheet pythondoc.css (fixes #63141).

 -- Matthias Klose <doko@cs.tu-berlin.de>  Thu,  4 May 2000 01:11:50 +0200

python-gnuplot (1.4-1) unstable; urgency=low

  * New upstream version. Somewhat incompatible: it's a package now.

 -- Matthias Klose <doko@cs.tu-berlin.de>  Mon, 25 Oct 1999 18:14:06 +0200

python-gnuplot (1.2-1) unstable; urgency=low

  * Initial Release.

 -- Matthias Klose <doko@cs.tu-berlin.de>  Sun, 22 Aug 1999 13:37:35 +0200

Local variables:
mode: debian-changelog
End:
